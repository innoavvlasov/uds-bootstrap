const db = require('../lib/news-db')
const News = db.model('User', {
  tableName: 'news'
})

async function getPublishedNews () {
  return News.fetchAll()
}

async function createPost (data) {
  return News.forge(data).save()
}

async function findById (id) {
  return News.where({ id: id }).fetch({ require: true })
}

async function deletePost (post) {
  return post.destroy()
}

async function update (post, data) {
  return post.save(data)
}

module.exports = {
  getPublishedNews,
  createPost,
  findById,
  deletePost,
  update
}
