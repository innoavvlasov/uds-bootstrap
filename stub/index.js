const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const fs = require("fs");
const path = require("path");

app.use(bodyParser.json());

// app.use(express.static("dist"));
app.use(express.static(__dirname + "/dist"));
app.use(express.static("stub/orgs/shared")); // open content dir to web

app.get(["/defaultApp.js"], function(request, response) {
  let reqPath = path.join(__dirname, "../");
  response.sendFile(
    path.resolve(reqPath, "node_modules/uds-login/dist/defaultApp.js")
  );
});

app.get(["/uds-orgs/orgsApp/organization/orion.js"], function(request, response) {
  let reqPath = path.join(__dirname, "../");
  response.sendFile(
    path.resolve(reqPath, "node_modules/uds-orgs/dist/orgsApp.js")
  );
});

app.use('/main', require('./main/main.controller'));
app.use('/news/api/news', require('./news/server/news/news.controller'));
app.use('/sections', require('./sections/sections.controller'));
app.use('/orgs', require('./orgs/orgs.controller'));
app.use('/users', require('./users/users.controller'));

app.listen(8090, () => console.log("Listening on port 8090!"));

module.exports = app;