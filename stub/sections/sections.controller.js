const express = require('express');
const router = express.Router();
const sectionsData = require('./data/sections');
const footballSection = require('./data/football_section');

/**
 returns list of sections in form:
 [
     { \\ новая секция
        "id": 1, \\ её айди
        "name": "Боевые искусства", \\ имя типа секций
        "sections": [ \\ список секций
            {
                "id": 2, \\ айди секции
                "name": "Каратэ", \\ имя секции
                "section_type": 1 \\ вот эта хрень это id объекта выше (вообще надо бы убрать)
            }
        ]
    }
 ]
 */
router.get('/all', async function getSections(req, res, next) {
    try {
        let result = [];
        sectionsData.forEach((section) => {
            result = [...result, ...section.sections]
        });
        return result ?
            res.json(result) :
            res.status(404).json({ message: 'Секции отсутствуют' });
        // const sectionsList = await sections.getSections();
        // return sectionsList ? res.json(sectionsList) : res.status(404).json({message: 'Секции отсутствуют'});
    } catch (error) {
        console.log(error);
        next(error);
    }
});

router.get('/byType', async function getSections(req, res, next) {
    try {
        return sectionsData ?
            res.json(sectionsData) :
            res.status(404).json({ message: 'Секции отсутствуют' });
        // const sectionsList = await sections.getSections();
        // return sectionsList ? res.json(sectionsList) : res.status(404).json({message: 'Секции отсутствуют'});
    } catch (error) {
        console.log(error);
        next(error);
    }
});

router.get('/detailedInfo/:section_id', async function getSection(req, res, next) {
    try {
        return footballSection ?
            res.json(footballSection) :
            res.status(404).json({ message: 'Секция отсутствует' });
    } catch (error) {
        console.log(error);
        next(error);
    }
});

/**
 либо 200 либо 500
 */
router.post('/section/edit', async function getSections(req, res, next) {
    try {
        console.log(`editing section with ${req.body.id} to "${req.body.value}"`);
        return Math.random() >= 0.5 ? res.status(200).json({}) : res.status(500).json({});
        // await sections.editSection(req.body.value, req.body.id);
    } catch (error) {
        console.log(error);
        next(error);
    }
});

/**
 либо 200 либо 500
 */
router.post('/section_type/edit', async function getSections(req, res, next) {
    try {
        console.log(`editing section type with ${req.body.id} to "${req.body.value}"`);
        return Math.random() >= 0.5 ? res.status(200).json({}) : res.status(500).json({});
        // await sections.editSectionType(req.body.value, req.body.id);
    } catch (error) {
        console.log(error);
        next(error);
    }
});

router.get('/section/:id', async function getSectionDetailed(req, res, next) {
    try {
        return footballSection ?
            res.json(footballSection) :
            res.status(404).json({ message: 'Секции отсутствуют' });
        // await sections.editSectionType(req.body.value, req.body.id);
    } catch (error) {
        console.log(error);
        next(error);
    }
});

module.exports = router;