module.exports = {
    "id": 1,

    "name": "Мини-футбол",

    "main_description": "Открыта запись в новые клубы и студии ГБУ \"Ратмир\"! Бесплатный тренажерный зал для взрослых и секции боевых искусств для детей от 4 лет.",

    "main_pic_url": "https://yandex.ru/images/search?pos=1&from=tabbar&img_url=https%3A%2F%2Fimages7.memedroid.com%2Fimages%2FUPLOADED931%2F58c0a6d3d960d.jpeg&text=si%20dog&rpt=simage",

    "description_title": "Что такое мини-футбол и в чём секрет его популярности",

    "description_body": "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана! \
    Давайте знакомиться, посмотрите видео и если останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24/7 и ответить на все вопросы. Мы предложим спортивные, \
    творческие и развивающие кружки и секции для детей.",

    "last_picture": "https://yandex.ru/images/search?pos=3&img_url=https%3A%2F%2Fpbs.twimg.com%2Fmedia%2FC-S0ywLXkAAy65o.jpg%3Alarge&text=%D1%87%D0%B8%D1%85%D1%83%D0%B0%D1%85%D1%83%D0%B0%20%D0%BC%D0%B5%D0%BC&rpt=simage"
};