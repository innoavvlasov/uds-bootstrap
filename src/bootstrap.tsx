import * as React from "react";
import { connect, ReactReduxContext } from 'react-redux';
import { listen } from 'fbjs/lib/EventListener';


import { openApp, errorOpenApp } from './workspace/actions';
import { loadApp } from './utils'

import { Wrapper } from './wrapper';

class Bootstrap extends React.PureComponent <any, any> {
    App: any;
    currentBundle: string = '';

    componentDidMount() {
        listen(window, 'popstate', this.onPopState);
    }

    onPopState = (event) => {
        this.props.openApp(window.location.hash.substr(1));
    }

    async downloader(bundleName: string) {
        let _bundleName = bundleName;
    //    console.log('bundleName', bundleName);
        this.currentBundle = bundleName;
        this.App = await loadApp(_bundleName);
        this.forceUpdate();
        window.history.pushState(
            { flowName: bundleName },
            '',
            `/#${bundleName}`
        );
    }

    render () {
        let { props: { bundleName, errorbundleName }, App, state, currentBundle } = this;
  
        if (bundleName !== currentBundle) {
            this.downloader(bundleName);
            return (<Wrapper><div>Идет загрузка...</div></Wrapper>);
        }
      
        return !App ? <Wrapper><div>Идет загрузка...</div></Wrapper> : <Wrapper {...this.props} reducer={App.reducer}><App {...this.props}/></Wrapper>;
    }
}

const mapStateToProps = (state) => {
    return {
        errorbundleName: state.workspace.errorbundleName,
        bundleName: state.workspace.bundleName || window.location.hash.substr(1) || 'defaultApp',
    }
};

const mapDispatchToProps = (dispatch) => ({
    openApp: (bundleName: string) => dispatch(openApp(bundleName)),
    errorOpenApp: (bundleName: string) => dispatch(errorOpenApp(bundleName))
    // getWorkSpace: () => dispatch(getWorkSpace()),
});

export const LoaderApp: any = connect(mapStateToProps, mapDispatchToProps)(Bootstrap);