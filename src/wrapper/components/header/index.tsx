import * as React from 'react';

import { LogoBlock, NavBlock, NavLink, PhoneBlock, PhoneText, Wrapper, NavLinkStub, MenuBlock, StyledLogo, StyledMenu } from './styled';
import { phone } from '../../assets';

export class Header extends React.PureComponent <any, any> {
  state = { menuOpen: false };

  handlerOpenMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
    document.body.style['overflow-y'] = !this.state.menuOpen ? 'hidden' : 'scroll';
  }

  navigateTo = (path) => {
    let _path = path;
    if (_path[0] === 'm') {
      this.handlerOpenMenu();
      _path = _path.substr(1);
    }

    // this.props.history.push(_path);
  }

  render () {
    return (
    <Wrapper>
      <LogoBlock to={'/'}>

        {!this.state.menuOpen && <StyledLogo />}
        {this.state.menuOpen && 'Меню'}

      </LogoBlock>
      <div><StyledMenu onClick={this.handlerOpenMenu} open={this.state.menuOpen} /></div>
      <NavBlock>
        <NavLink onClick={() => this.props.openApp('uds-main/mainApp')}  >Главная</NavLink>
        <NavLink onClick={() => this.props.openApp('uds-orgs/orgsApp')}  >Учреждения</NavLink>
        <NavLink onClick={() => this.props.openApp('uds-news/newsApp')} >Новости</NavLink>
        <NavLink onClick={() => this.props.openApp('uds-sections/sectionsApp')} >Секции</NavLink>
        {/* <NavLinkStub >Рейтинги</NavLinkStub> */}
        <PhoneBlock>
          <div>
            <img src={phone} alt="phone" />
          </div>
          <PhoneText href="tel:+74952307071">8 (495) 230-70-71</PhoneText>
        </PhoneBlock>
      </NavBlock>
      {this.state.menuOpen && (<MenuBlock>

          <NavLink onClick={() => this.navigateTo('m/')}  >Главная</NavLink>
          <NavLink onClick={() => this.navigateTo('m/organizations')}  >Учреждения</NavLink>
          <NavLink onClick={() => this.navigateTo('/news')} >Новости</NavLink>
          <NavLink onClick={() => this.navigateTo('m/sections')}  >Секции</NavLink>
        <PhoneBlock>
          <div>
            <img src={phone} alt="phone" />
          </div>
          <PhoneText href="tel:+74952307071">8 (495) 230-70-71</PhoneText>
        </PhoneBlock>

      </MenuBlock>)}
    </Wrapper>
    );
  }
}
