import React, { useContext } from "react";
import MetaTags from 'react-meta-tags';
import { ReactReduxContext } from 'react-redux';

import { injectAsyncReducer } from '../store';
import { socialLinks } from './constants';

import { Header } from './components/header';
import { Footer } from './components/footer';


export const Wrapper = (props) => {
console.log('props', props);
    if(props.reducer) {
        const { store } = useContext(ReactReduxContext)
        injectAsyncReducer(store, 'app', props.reducer);
    }

    return (<React.Fragment>
        <Header {...props} />
        {props.children}
        <Footer links={socialLinks} />
    </React.Fragment>)
}
