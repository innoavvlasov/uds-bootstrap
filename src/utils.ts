export async function loadApp(bundleName) {
    // console.log(`./${bundleName}.js`);
    return await System.import(`./${bundleName}.js`)
        .then(result => {
            return result.default;
        })
        .catch(error => {
            console.info(`Загрузить ${bundleName} не удалось!`);
            console.info(`error in ${bundleName}`, error);
        });
}