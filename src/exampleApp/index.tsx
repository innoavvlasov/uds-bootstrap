import * as React from "react";
import { connect } from 'react-redux';

class ExampleApp extends React.PureComponent<any, any> {
    render () {
        // console.log('app.props', this.props);

        return <div> 
            <button onClick={() => this.props.openApp('uds-news/newsApp')}>Команда новостей</button>
            <button onClick={() => this.props.openApp('uds-sections/sectionsApp')}>Команда секций</button>
            <button onClick={() => this.props.openApp('uds-orgs/orgsApp')}>Команда организаций</button>
        </div>
    }
}
const connectedApp = connect(null)(ExampleApp)
connectedApp['reducer'] = () => ({a:1}) ;
export default connectedApp;